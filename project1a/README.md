# Project 1A: Barrel Bouncer (Setup)

For most of this semester, you will be working on a first-person 3D game called Barrel Bouncer, where you score points by throwing balls to destroy barrels in your environment. Project 1 deals with developing this game in the Unity editor for your computer, while projects 2 & 3 deal with transforming the game into a VR and AR/MR experience, respectively.

This first part of project 1 will begin developing your Unity skills by getting you used to working with the Unity Editor and some basic C# scripting. You will learn how to find and import existing 3D models using the Unity Asset Store and online sources and then use them to build out a custom environment utlizing the scene view and the transform tools. Then, you will design a way for a user to move and fly around this environment from a first-person perspective.


* [Part 0: Creating the Project](create-project)
* [Part 1: Imagining Your World](imagine-world)
* [Part 2: Finding & Importing 3D Models](import-models)
* [Part 3: Designing Your Scene](design-scene)
* [Part 4: Allowing Users to Move Around](basic-movement)
* [Part 5: Submitting Your Project](submission)
