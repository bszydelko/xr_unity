# Project 2: Barrel Bouncer VR Edition

In project 1, you applied your knowledge of basic Unity concepts to develop a completed game from scratch. In this project, you will be taking that game and transforming it into a VR experience by adding in some common interactions with the headsets and motion controllers while adhering to some of the best performance and UX practices for virtual reality.

* [Part 0: Notes About the SDKs and the Hardware](notes)
* [Part 1: Setting Up Your Project for VR](setup)
* [Part 2: Modifications to Your Environment](environment-mods)
* [Part 3: Movement in VR](locomotion)
* [Part 4: Throwing Balls with Motion Controllers](throw-balls)
* [Part 5: Adjusting the UI](ui)
* [Part 6: A More Realistic Spatial Audio Experience](audio)
* [Part 7: Submitting Your Project](submission)
